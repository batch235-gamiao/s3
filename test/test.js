const chai = require("chai");
const { assert } = require("chai");

// import and use chai-http to allow chai to send requests to our server
const http = require("chai-http");
chai.use(http);

describe("API Test Suite for users", () => {
	
	it("Test API get users is running", (done) => {

		// request() method is used from chai to create an http request given to the server
		// get("/endpoint") method is used to run/ access a get method route
		// end() method is used to access the response from the route. It has anonymous function as an argument that receives 2 objects, the err or the response
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			// isDefined is assertion that given data is not undefined. It's like shortcut to .notEqual(typeof data, undefined)

			assert.isDefined(res);

			// done() method is used to tell chai-http when the test is done
			done();
		})
	})

	it("Test API get users returns an array", (done) => {
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err,res) => {

			// res.body contains the body of the response. The data sent from res.
			// isArray() is an assertion that the given data is an array
			assert.isArray(res.body);
			
			done();
		})
	})

	it("Test API get users array first object username is Jojo", (done) => {

		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {

			assert.equal(res.body[0].username, "Jojo");
			done();
		})
	})

	// Mini-Activity #2
	it("Test API get users array last item is not undefined", (done) => {

		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {

			assert.isDefined(res.body[res.body.length - 1]);
			done();
		})
	})
	//

	it("Test API post users returns 400 if no name", (done) => {

		// post() which is used by chai http to access a post method route
		// type() which is used to tell chai that request body is going to be stringified as a json
		// send() is used to send the request body
		chai.request("http://localhost:4000")
		.post("/users")
		.type("json")
		.send({
			age: 30,
			username: "jin92"
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	//Mini-Activity #3
	it("Test API post users returns 400 if no age", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			name:"hello",
			username: "jin92"
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})
})

// Activity s3
describe("API Test Suite for artists", () => {

	it("Test API get artists is running", (done) => {

		chai.request("http://localhost:4000")
		.get("/artists")
		.end((err, res) => {

			assert.isDefined(res);
			done();
		})
	})

	it("Test API get artists first object returns an array", (done) => {
		chai.request("http://localhost:4000")
		.get("/artists")
		.end((err,res) => {

			assert.isArray(res.body[0].songs);
			done();
		})
	})

	it("Test API post artists returns 400 if no name", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
				songs: ["Perfect", "Happier"],
				album: "÷",
				isActive: true
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	it("Test API post artists returns 400 if no songs", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({			
				name: "Ed Sheeran",
				album: "÷",
				isActive: true
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	it("Test API post artists returns 400 if no album", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({			
				name: "Ed Sheeran",
				songs: ["Perfect", "Happier"],
				isActive: true
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	it("Test API post artists returns 400 if isActive property given is false", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({			
				name: "Ed Sheeran",
				songs: ["Perfect", "Happier"],
				album: "÷",
				isActive: false
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})
})
//