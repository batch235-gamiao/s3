// Mini-Activity #1
const  express = require("express");
const app = express();
const PORT = 4000;
//

app.use(express.json());

let users = [
	{
		name: "Jojo Joestar",
		age: 25,
		username: "Jojo"
	},
	{
		name: "Dio Brando",
		age: 23,
		username: "Dio"
	},
	{
		name: "Jotaro Kujo",
		age: 28,
		username: "Jotaro"
	}
]

// Activity s3
let artists = [
	{
		name: "Bruno Mars",
		songs: ["24k Magic", "Versace on the floor"],
		album: "24k Magic",
		isActive: true
	},
	{
		name: "Taylor Swift",
		songs: ["Fearless", "Love Story"],
		album: "Fearless",
		isActive: true
	},
	{
		name: "John Legend",
		songs: ["Love in the future", "All of me"],
		album: "Love in the future",
		isActive: true
	}
]
// 


// [SECTION] Routes and Controllers for USERS

app.get("/users", (req, res) => {
	console.log(users);
	return res.send(users);
});

app.post("/users", (req, res) => {

	// add simple if statement that if the request body does not have property name, we will send message along with a 400 http status code (Bad Request)
	// hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}
	if(!req.body.hasOwnProperty("age")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}
})

// Activity s3
app.get("/artists", (req, res) => {
	console.log(artists);
	return res.send(artists);
})
app.post("/artists", (req, res) => {
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}
	if(!req.body.hasOwnProperty("songs")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter SONGS"
		})
	}
	if(!req.body.hasOwnProperty("album")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter ALBUM"
		})
	}
	if(req.body.isActive === false){
		return res.status(400).send({
			error: "isActive property is false"
		})
	}
})
// 

app.listen(PORT, () => console.log(`Running on port ${PORT}`));
 